﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Timers;
using System.Threading;
using System.Diagnostics;
using System.ServiceProcess;
using System.IO.Compression;
using System.Text.RegularExpressions;

namespace STPromax
{
    public partial class STPromax : ServiceBase
    {
        //Main
        private static STPromaxConsole stp = new STPromaxConsole();
        //Timer
        private static System.Timers.Timer t = null;
        private static int c = 0;
        //Controle        
        private static bool running = false;
        //static string hostname;
        //Controle de processos
        private static StringBuilder sortOutput = null;
        private static int numberOfProcess = 8;
        private static string stopFile = AppDomain.CurrentDomain.BaseDirectory + "stop";

        public STPromax()
        {
            InitializeComponent();

            t = new System.Timers.Timer(1000);
            t.Elapsed += new ElapsedEventHandler(t_Elapsed);
        }

        protected override void OnStart(string[] args)
        {
            //Exclui arquivo de controle
            //STPromaxConsole.DeleteFileNotRaise(stopFile);
            stp.DeleteFileNotRaise(stopFile);
            //Comprime Logs
            CompressLogs();
            //Grava log
            LogMessage(1, "Iniciando serviço.");
            //Garante que não existe nenhum processo zumbi
            KillAll();
            //Inicia o timer do serviço
            t.Start();
            //Grava log
            LogMessage(1, "Serviço iniciado.");
        }

        protected override void OnStop()
        {
            LogMessage(1, "Parando serviço.");
            t.Stop();
            (File.Open(stopFile, FileMode.Create)).Close();
            Thread.Sleep(5000);
            KillAll();
            LogMessage(1, "Serviço parado.");
        }

        protected override void OnShutdown()
        {
            OnStop();
        }

        protected override void OnPause()
        {
            t.Stop();
            (File.Open(stopFile, FileMode.Create)).Close();
            Thread.Sleep(5000);
            //KillAll();
            LogMessage(1, "Serviço em pausa.");
        }

        protected override void OnContinue()
        {
            //STPromaxConsole.DeleteFileNotRaise(stopFile);
            stp.DeleteFileNotRaise(stopFile);
            t.Start();
            LogMessage(1, "Serviço reiniciado.");
        }

        private void t_Elapsed(object sender, EventArgs e)
        {
            if (!running)
            {
                running = true;
                if (c % 360 == 0)
                {
                    //STPromaxConsole.LoadConfig();
                    stp.LoadConfig();
                    CompressLogs();
                    c = 0;
                }
                try
                {
                    //numberOfProcess = STPromaxConsole.Config().ProcessosParalelos;
                    numberOfProcess = stp.Config().ProcessosParalelos;
                    MainLoop();
                    //GC.Collect();
                }
                catch (Exception ex)
                {
                    LogMessage(900, String.Format("Ocorreu um erro no processamento! ({0})", ex.Message));
                    ServiceController sc = new ServiceController("STPromax");
                    sc.Stop();
                }
                finally
                {
                    running = false;
                    c++;
                }
            }
        }
        
        private static void CompressLogs()
        {
            ThreadStart compress_start = new ThreadStart(CompressingLogs);
            Thread compress_log = new Thread(compress_start);
            //Comprime log's anteriores
            compress_log.Start();
        }

        private static void KillAll()
        {
            int id;

            //Se existirem processos executando, cria arquivo de stop
            if (RunningProcesses() > 0)
            {
                try
                {
                    if (!File.Exists(stopFile))
                    {
                        (File.Open(stopFile, FileMode.Create)).Close();
                        Thread.Sleep(5000);
                        File.Delete(stopFile);
                    }
                }
                catch
                {

                }
            }
            //O código abaixo recupera a lista de todos os processos abertos SDSIVConsole e mata
            Process[] processos = Process.GetProcessesByName("STPromaxConsole");
            foreach (Process processo in processos)
            {
                //Mata processos em execução ...                
                id = processo.Id;
                try
                {
                    processo.Kill();
                }
                catch
                {

                }
                finally
                {
                    LogMessage(1, String.Format("O processo Id ({0}) morto...", id));
                }
            }
        }

        private static int RunningProcesses()
        {
            int processCount = 0;

            Process[] processos = Process.GetProcessesByName("STPromaxConsole");
            foreach (Process processo in processos)
                processCount++;

            return processCount;
        }

        private static void MainLoop()
        {
            int i;
            int processCount = 0;

            if (sortOutput == null)
                sortOutput = new StringBuilder("");

            try
            {
                Process[] processos = Process.GetProcessesByName("STPromaxConsole");
                foreach (Process processo in processos)
                {
                    processCount++;
                    if (File.Exists(stopFile))
                        continue;

                    if (!processo.Responding)
                    {
                        int id = processo.Id;
                        LogMessage(1, String.Format("O processo Id ({0}) não está respondendo !", id));
                        processo.Kill();
                        LogMessage(1, String.Format("O processo Id ({0}) morto...", id));
                    }
                }

                if (processCount != numberOfProcess)
                {
                    if (processCount < numberOfProcess)
                    {
                        for (i = 0; i < numberOfProcess - processCount; i++)
                        {
                            StartSTPromaxConsole(AppDomain.CurrentDomain.BaseDirectory, "STPromaxConsole.exe");
                            Thread.Sleep(1000);
                        }
                    }
                    else
                        KillAll();
                }
                //Grava log
                lock (sortOutput)
                {
                    LogMessage(sortOutput.ToString());
                    sortOutput.Length = 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        private static void LogMessage(string Message)
        {
            string logfilename = STPromaxConsole.LogFileName();

            FileStream logfile = File.Open(logfilename, FileMode.Append);
            StreamWriter l = new StreamWriter(logfile);            
            l.Write(String.Format("{0}", Message));
            l.Close();
            logfile.Close();
        }

        private static void OutputLogHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //Process p = (Process)sendingProcess;
            //Regex regex = new Regex(@"XXXXX");
            //string data = regex.Replace(outLine.Data, p.Id.ToString());

            //string[] lines = data.Split('\n');
            string[] lines = outLine.Data.Split('\n');
            foreach (string line in lines)
            {
                string l = line.Trim();
                if (!String.IsNullOrEmpty(l))
                    sortOutput.AppendLine(l);
            }
        }

        private static Process StartSTPromaxConsole(string basedir, string processname)
        {
            Process proc = new Process();

            proc.StartInfo.FileName = basedir + processname;
            proc.StartInfo.WorkingDirectory = basedir;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
            proc.OutputDataReceived += new DataReceivedEventHandler(OutputLogHandler);
            proc.Start();
            //Inicia a captura da saída do processo
            proc.BeginOutputReadLine();

            LogMessage(1, String.Format("Novo processo iniciado (Id = {0}).", proc.Id));

            return proc;
        }

        private static bool IsFileOpen(string filePath)
        {
            bool result = false;
            try
            {
                System.IO.FileStream fs = System.IO.File.OpenWrite(filePath);
                fs.Close();
            }
            catch //(System.IO.IOException ex)
            {
                result = true;
            }
            return result;
        }

        static void CompressingLogs()
        {
            if (CompressLogFiles())
                LogMessage(4, "Arquivos compactados com sucesso.");
            else
                LogMessage(904, "Arquivos compactados com erro!");
        }

        private static bool CompressLogFiles()
        {
            string[] files;
            string LogPath;
            string logfilename = STPromaxConsole.LogFileName();

            LogPath = AppDomain.CurrentDomain.BaseDirectory;
            files = Directory.GetFiles(LogPath, "*.log");
            foreach (string zipfile in files)
            {
                if (zipfile == logfilename)
                    continue;
                try
                {
                    string outzipfile;
                    //string anyString = File.ReadAllText(zipfile);
                    LogMessage(4, String.Format("Compactando aquivo ({0} => {0}.gz)", zipfile));
                    if(CompressFileToFile(zipfile, out outzipfile))
                        LogDeleteFile(zipfile);
                    else
                        LogMessage(4, String.Format("Abortando compactar aquivo ({0} => {0}.gz)", zipfile));
                }
                catch (Exception e)
                {
                    LogMessage(904, String.Format("Ocorreu erro ao compactar aquivo {1} ({0})", e.Message, zipfile));

                    return false;
                }

                if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "stop"))
                    return true;
            }

            return true;
        }

        private static bool CompressFileToFile(string inputFile, out string outputFile)
        {
            bool result = false;
            int size = 64 * 1024; //64k
            outputFile = inputFile + ".gz";

            byte[] b = new byte[size];
            FileStream fi = new FileStream(inputFile, FileMode.Open);
            FileStream fo = new FileStream(outputFile, FileMode.Create);
            GZipStream gz = new GZipStream(fo, CompressionMode.Compress, true);
            long total = fi.Length;            
            try
            {
                while (total > size)
                {
                    fi.Read(b, 0, size);
                    gz.Write(b, 0, size);
                    total -= size;
                }
                result = true;
            }
            catch (Exception e)
            {
                LogMessage(1, String.Format("Erro ao compactar arquivo: {0} ({1})", outputFile, e.Message));
            }
            finally
            {
                gz.Write(b, 0, (int)total);
                gz.Close();
                fi.Close();
                fo.Close();
            }

            return result;
        }

        private static bool CompressStringToFile(string fileName, string value)
        {
            if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "stop"))
                return false;

            // A.
            // Write string to temporary file.
            string temp = Path.GetTempFileName();
            File.WriteAllText(temp, value);

            if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "stop"))
                return false;

            // B.
            // Read file into byte array buffer.
            byte[] b;
            using (FileStream f = new FileStream(temp, FileMode.Open))
            {
                b = new byte[f.Length];
                f.Read(b, 0, (int)f.Length);
            }

            if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "stop"))
                return false;

            // C.
            // Use GZipStream to write compressed bytes to target file.
            using (FileStream f2 = new FileStream(fileName, FileMode.Create))
            using (GZipStream gz = new GZipStream(f2, CompressionMode.Compress, false))
            {
                gz.Write(b, 0, b.Length);
            }

            return true;
        }

        private static void LogMessage(int evento, string Message)
        {
            string now;
            string logmessage;
            string logfilename = 
                String.Format(
                    "{0}service_{1:D4}{2:D2}{3:D2}.txt",
                    AppDomain.CurrentDomain.BaseDirectory,
                    DateTime.Now.Year,
                    DateTime.Now.Month,
                    DateTime.Now.Day
                );
            try
            {
                now = String.Format("{0:dd-MM-yyyy HH:mm:ss,FFFFFFF}", DateTime.Now);
                now = now.PadRight(27, '0');

                logmessage =
                    String.Format(
                        "{0}|{1}|{2:000000}|{3:000000}|{4:000}|{5:0000}|{6:0000}|{7:000}|{8:000}|{9}|{10}\n",
                        now,                                    //00
                        Dns.GetHostName(),                      //01
                        Process.GetCurrentProcess().Id,         //02
                        Thread.CurrentThread.ManagedThreadId,   //03
                        evento,                                 //04
                        0,                                      //05
                        0,                                      //06
                        0,                                      //07
                        0,                                      //08
                        now,                                    //09
                        Message.TrimEnd('\n').TrimEnd('\r')     //10
                    );
                
                /* IF DEBUG SERVICE
                FileStream logfile = File.Open(logfilename, FileMode.Append);                                
                StreamWriter l = new StreamWriter(logfile);
                l.WriteLine(String.Format("{0}", logmessage));
                l.Close();
                logfile.Close();
                ELSE*/                
                LogMessage(logmessage);
            }
            catch
            {
                //Inibe erro para não travar o serviço
            }
        }

        public static bool LogDeleteFile(string f)
        {
            bool r = true;
            try
            {
                System.IO.File.Delete(f);
                LogMessage(6, String.Format("Excluiu arquivo: {0}", f));
            }
            catch (Exception ex)
            {
                LogMessage(906, String.Format("Erro ao excluir arquivo de {0}. ({1})", f, ex.Message));
                r = false;
            }
            return r;
        }
    }
}
