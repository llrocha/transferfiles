﻿namespace STPromax
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.svcProcessInstallSTPromax = new System.ServiceProcess.ServiceProcessInstaller();
            this.svcInstallSTPromax = new System.ServiceProcess.ServiceInstaller();
            // 
            // svcProcessInstallSTPromax
            // 
            this.svcProcessInstallSTPromax.Account = System.ServiceProcess.ServiceAccount.NetworkService;
            this.svcProcessInstallSTPromax.Password = null;
            this.svcProcessInstallSTPromax.Username = null;
            // 
            // svcInstallSTPromax
            // 
            this.svcInstallSTPromax.Description = "Serviço para envio de descargas dos PDAs para o Promax";
            this.svcInstallSTPromax.DisplayName = "Serviço de Transferência Promax";
            this.svcInstallSTPromax.ServiceName = "STPromax";
            this.svcInstallSTPromax.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.svcProcessInstallSTPromax,
            this.svcInstallSTPromax});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller svcProcessInstallSTPromax;
        private System.ServiceProcess.ServiceInstaller svcInstallSTPromax;
    }
}