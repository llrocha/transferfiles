﻿using System;
using System.Threading;

namespace STPromaxConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string stopfile = AppDomain.CurrentDomain.BaseDirectory + "stop";
            STPromax.STPromaxConsole configLoader = new STPromax.STPromaxConsole();
            ParameterizedThreadStart runner = new ParameterizedThreadStart(runParam);
            Thread[] threads = null;
            int no_threads = 0;
            int c = 0;

            while (true)
            {
                if (c % 36000 == 0)
                {
                    c = 0;
                    configLoader.LoadConfig();
                    if (no_threads != configLoader.Config().geos.Count)
                    {
                        no_threads = configLoader.Config().geos.Count;
                        threads = new Thread[no_threads];
                    }
                }
                try
                {
                    if (c % 600 == 0)
                        GC.Collect();

                    for (int i = 0; i < no_threads; i++)
                    {
                        if (threads[i] == null)
                        {
                            threads[i] = new Thread(runner);                            
                            threads[i].Start(i);
                            Thread.Sleep(100);
                        }
                        else
                        if (threads[i].ThreadState == ThreadState.Stopped)
                        {                            
                            threads[i] = null;
                            threads[i] = new Thread(runner);
                            threads[i].Start(i);
                            Thread.Sleep(100);
                        }
                    }
                }
                catch (Exception ex)
                {
                    STPromax.STPromaxConsole.LogMessage(900, 0, 0, String.Format("Ocorreu um erro no processamento! ({0})", ex.Message));
                    return;
                }
                finally
                {
                    Thread.Sleep(100);
                    c++;                    
                }
                if (System.IO.File.Exists(stopfile))
                {
                    STPromax.STPromaxConsole.LogMessage(900, 0, 0, String.Format("Encontrado: {0}", stopfile));
                    return;
                }
            }
        }

        static void runParam(Object o)
        {
            STPromax.STPromaxConsole runParam = new STPromax.STPromaxConsole();
            runParam.MainLoopParam(o);            
        }
    }
}