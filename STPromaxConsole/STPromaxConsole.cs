﻿using System;
using System.IO;
using System.Net;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Data.SqlClient;

using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;


namespace STPromax
{
    public class STPromaxConsole
    {
        //Configuracao
        static STPromaxIniLoader loader = null;
        static STPromaxConfig config = null;
        string UNB_GEO = null;


        //Log
        static string hostname = Dns.GetHostName();
        static bool Log2Console = true;

        public STPromaxConsole()
        {

        }

        public STPromaxConfig Config()
        {
            if (loader == null)
                loader = new STPromaxIniLoader();

            if (config == null)
                config = loader.Config();

            return config;
        }

        public string[] getSortedFilesByCreationTime(string RootPath, string searchPattern)
        {
            string[] files;
            DirectoryInfo directory = new DirectoryInfo(RootPath);
            IEnumerable<FileInfo> zips = directory.EnumerateFiles(searchPattern);

            DateTime[] creationTimes = new DateTime[zips.Count()];
            files = new string[zips.Count()];

            int i = 0;
            foreach (var zip in zips)
            {
                creationTimes[i] = zip.CreationTime;
                files[i] = zip.Name;
                i++;
            }
            Array.Sort(creationTimes, files);

            return files;
        }

        public void MainLoopParam(Object o)
        {
            int i = Int32.Parse(o.ToString());            
            UNB_GEO = Config().geos[i];
            try
            {
                STPromax.STPromaxConsole.LogMessage(1, Int32.Parse(UNB_GEO), 0, String.Format("Criando Thread GEO ({0})", i));
                MainLoop();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                STPromax.STPromaxConsole.LogMessage(1, Int32.Parse(UNB_GEO), 0, String.Format("Finalizando Thread GEO ({0})", i));
            }
        }

        public void MainLoop()
        {
            int counter = 240; // 1 min +/-
            string RootPath;
            string lockfilename;
            FileStream lockfile;

            //Arquivos que serão processados
            string[] files;
            int cddfiles;

            //Cria arquivo hearbeat
            CreateHeartBeatFile();

            while (counter > 0)
            {
                //Loop de processamento
                foreach (PromaxServer cdd in Config().sivs)
                {
                    //Somente trata CDD's da GEO
                    if (!cdd.CodGeo.Equals(UNB_GEO))
                        continue;

                    int geo = Int32.Parse(UNB_GEO);
                    int unb = Int32.Parse(cdd.CodUnb);

                    /*
                    LogMessage(6, geo, unb,
                        String.Format(
                            "GEO: {0:D4} CDD: {1:D4}",
                            Int32.Parse(cdd.CodGeo),
                            Int32.Parse(cdd.CodUnb)
                        )
                    );
                    */

                    //Caminho
                    RootPath = Config().AfariaPath + "\\" + cdd.CodUnb + "\\SIV\\Vendedor\\descarga\\";

                    if (!Directory.Exists(RootPath))
                    {
                        LogMessage(902, geo, 0, String.Format("Diretório não existe {0}", RootPath));
                        continue;
                    }

                    //Tenta criar arquivo de LOCK para processar o CDD, caso não consiga pula o CDD
                    lockfilename = RootPath + "process.lck";
                    try
                    {
                        lockfile = File.Open(lockfilename, FileMode.OpenOrCreate, FileAccess.Write);
                    }
                    catch
                    {
                        //Saltando CDD, pois já tem uma thread processando
                        continue;
                    }

                    try
                    {
                        //Processando arquivos ZIP 
                        files = getSortedFilesByCreationTime(RootPath, "*.zip");

                        if (files.Length > 0)
                            LogMessage(6, geo, unb,
                                String.Format(
                                    "Encontrado(s) {0} arquivo(s) para o CDD: {1:D4}",
                                    files.Length,
                                    Int32.Parse(cdd.CodUnb)
                                )
                            );

                        cddfiles = 0;
                        foreach (string file in files)
                        {
                            DateTime c, i;
                            string[] s;
                            string f;
                            string m;
                            int area;
                            int setor;

                            if (cddfiles++ > 15)
                                return;

                            s = file.Split('\\');
                            f = s[s.Length - 1]; //Arquivo sem caminho

                            area = Int32.Parse(f.Substring(0, 3));
                            setor = Int32.Parse(f.Substring(3, 3));

                            m = String.Format("Processando o arquivo: {0}", f);
                            LogMessage(6, geo, unb, area, setor, file, m);
                            i = DateTime.Now;
                            c = File.GetCreationTime(file);
                            if (ProcessZip(RootPath, f, cdd))
                            {
                                s = file.Split('\\');
                                m = String.Format("Movendo o arquivo: {0} para {1}\\backup\\{0}", f, RootPath);
                                LogMessage(6, geo, unb, area, setor, file, m);
                                LogCopyFile(geo, unb, area, setor, RootPath + "\\" + f, RootPath + "\\backup\\" + f);
                                LogDeleteFile(geo, unb, area, setor, RootPath + "\\" + f);
                            }
                            TimeSpan difi = DateTime.Now - i;
                            TimeSpan difc = DateTime.Now - c;
                            m = String.Format(
                                    "Arquivo: {0} - CDD: {5:D4} - Processamento: {1:D2}:{2:D2}:{3:D2},{4:D3} - Espera: {6:D2}:{7:D2}:{8:D2},{9:D3}",
                                    f,
                                    difi.Hours,
                                    difi.Minutes,
                                    difi.Seconds,
                                    difi.Milliseconds,
                                    Int32.Parse(cdd.CodUnb),
                                    difc.Hours,
                                    difc.Minutes,
                                    difc.Seconds,
                                    difc.Milliseconds
                                );
                            LogMessage(6, geo, unb, area, setor, file, m);

                            if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "stop"))
                                return;
                        }

                        if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "stop"))
                            return;
                    }
                    finally
                    {
                        lockfile.Close();
                        DeleteFileNotRaise(lockfilename);
                    }
                    Thread.Sleep(250);
                }
                counter--;
            }
        }

        bool ProcessZip(string RootPath, string zip, PromaxServer cdd)
        {
            bool OK;
            int geo = Int32.Parse(cdd.CodGeo);
            int unb = Int32.Parse(cdd.CodUnb);
            int area, setor;
            //string ErrorMsg;
            string Area, Sector;
            string Path, File;
            string[] s;

            string from = RootPath;
            string topromax = cdd.PromaxServidorDescarga;

            string ZIPFile;
            string ZIPFileRQT;
            string Day = DateTime.Now.Day.ToString();
            string Month = DateTime.Now.Month.ToString();
            string Year = DateTime.Now.Year.ToString();

            if (Day.Length == 1)
                Day = "0" + Day;
            if (Month.Length == 1)
                Month = "0" + Month;

            //ErrorMsg = "";
            s = zip.Split('\\');
            File = s[s.Length - 1];
            Area = File.Substring(0, 3);
            Sector = File.Substring(3, 3);
            area = Int32.Parse(Area);
            setor = Int32.Parse(Sector);
            if (!RootPath.EndsWith("\\"))
                RootPath += "\\";
            Path = RootPath + Area + "\\" + Sector;

            ZIPFile = Area + Sector + ".ZIP";
            ZIPFileRQT = ZIPFile + ".RQT";

            //Move *.ZIP to Promax
            OK = false;
            try
            {
                if (System.IO.File.Exists(from + ZIPFile))
                {
                    if (System.IO.File.Exists(topromax + ZIPFileRQT))
                        LogDeleteFile(geo, unb, area, setor, topromax + ZIPFileRQT);
                    if (LogCopyFile(geo, unb, area, setor, from + ZIPFile, topromax + ZIPFile))
                    {
                        LogCreateFile(geo, unb, area, setor, topromax + ZIPFileRQT);
                        OK = true;
                    }
                }
                else
                    LogMessage(906, geo, unb, "Não existe arquivo: " + from + ZIPFile);

            }
            catch (Exception e)
            {
                LogMessage(906, geo, unb, e.Message);
            }

            return OK;
        }

        public static bool LogCreateFile(int geo, int cdd, int area, int setor, string f)
        {
            bool r = true;
            try
            {
                System.IO.File.Create(f);
                LogMessage(6, geo, cdd, area, setor, f, String.Format("Criou arquivo: {0}", f));
            }
            catch (Exception ex)
            {
                LogMessage(906, geo, cdd, area, setor, f, String.Format("Erro ao criar arquivo de {0}. ({1})", f, ex.Message));
                r = false;
            }
            return r;
        }

        public static bool LogCopyFile(int geo, int cdd, int area, int setor, string f, string t)
        {
            bool r = true;
            try
            {
                FileInfo b = new FileInfo(f);
                DirectoryInfo d = new DirectoryInfo(b.DirectoryName);

                FileInfo c = new FileInfo(t);
                DirectoryInfo e = new DirectoryInfo(c.DirectoryName);

                d.Refresh();
                e.Refresh();
                if (!(d.Exists && e.Exists))
                {
                    if (!d.Exists)
                        throw new Exception(b.DirectoryName + " não disponível!");
                    if (!e.Exists)
                        throw new Exception(c.DirectoryName + " não disponível!");
                }
                else
                    System.IO.File.Copy(f, t, true);

                LogMessage(6, geo, cdd, area, setor, f, String.Format("Copiou arquivo: {0} para {1}", f, t));
            }
            catch (Exception ex)
            {
                LogMessage(906, geo, cdd, area, setor, f, String.Format("Erro ao copiar arquivo de {0} para {1}. ({2})", f, t, ex.Message));
                r = false;
            }
            return r;
        }

        public static bool LogDeleteFile(int geo, int cdd, int area, int setor, string f)
        {
            bool r = true;
            try
            {
                System.IO.File.Delete(f);
                LogMessage(6, geo, cdd, area, setor, f, String.Format("Excluiu arquivo: {0}", f));
            }
            catch (Exception ex)
            {
                LogMessage(906, geo, cdd, area, setor, f, String.Format("Erro ao excluir arquivo de {0}. ({1})", f, ex.Message));
                r = false;
            }
            return r;
        }

        void CreateHeartBeatFile()
        {
            string heartbeatfilename =
                AppDomain.CurrentDomain.BaseDirectory + "imlive.heartbeat";

            if (!File.Exists(heartbeatfilename))
            {
                FileStream heartbeatfile = null;
                try
                {
                    heartbeatfile = File.Open(heartbeatfilename, FileMode.Create);
                }
                catch
                {

                }
                finally
                {
                    if (heartbeatfile != null)
                        heartbeatfile.Close();
                }
            }
        }

        bool IsFileOpen(string filePath)
        {
            bool rtnvalue = false;
            try
            {
                System.IO.FileStream fs = System.IO.File.OpenWrite(filePath);
                fs.Close();
            }
            catch //(System.IO.IOException ex)
            {
                rtnvalue = true;
            }
            return rtnvalue;
        }

        public static void LogMessage(int evento, int geo, int cdd, string Message)
        {
            LogMessage(evento, geo, cdd, 0, 0, "", Message);
        }

        public static string LogFileName()
        {
            return String.Format(
                    "{0}stpromax_{1:D4}{2:D2}{3:D2}.log",
                    AppDomain.CurrentDomain.BaseDirectory,
                    DateTime.Now.Year,
                    DateTime.Now.Month,
                    DateTime.Now.Day
                );
        }

        static void LogMessage(int evento, int geo, int cdd, int area, int setor, string filename, string Message)
        {
            string now, creation;
            string logmessage;
            string logfilename = LogFileName();
            try
            {
                DateTime creationTime;
                if (File.Exists(filename))
                {
                    creationTime = new FileInfo(filename).CreationTime;
                    //Message += String.Format("|{0}", filename);
                }
                else
                    creationTime = DateTime.Now;

                now = String.Format("{0:dd-MM-yyyy HH:mm:ss,FFFFFFF}", DateTime.Now);
                now = now.PadRight(27, '0');

                creation = String.Format("{0:dd-MM-yyyy HH:mm:ss,FFFFFFF}", creationTime);
                creation = creation.PadRight(27, '0');

                logmessage =
                    String.Format(
                        "{0}|{1}|{2:000000}|{3:000000}|{4:000}|{5:0000}|{6:0000}|{7:000}|{8:000}|{9}|{10}",
                        now,                                    //00
                        hostname,                               //01
                        Process.GetCurrentProcess().Id,         //02
                        Thread.CurrentThread.ManagedThreadId,   //03
                        evento,                                 //04
                        geo,                                    //05
                        cdd,                                    //06
                        area,                                   //07
                        setor,                                  //08
                        creation,                               //09
                        Message                                 //10
                    );

                if (Log2Console)
                {
                    Console.WriteLine(logmessage);
                }
            }
            catch
            {
                //Inibe erro para não travar o serviço
            }
        }

        void RemoveExceptionList(List<PromaxServer> sivs, List<int> ExclusionSIV)
        {
            foreach (int cdd in ExclusionSIV)
            {
                foreach (PromaxServer siv in sivs)
                {
                    if (siv.CodUnb.Trim() == cdd.ToString())
                    {
                        sivs.Remove(siv);
                        break;
                    }
                }
            }
        }

        string QueryPromax(PromaxServer promax, int query)
        {
            string unb = promax.CodUnb;
            string promax_address = promax.PromaxServidor;
            string responseFromServer = "";

            try
            {
                string url = "{0}/PP00100.exe?ppopcao=55&requisicao=5&empresaPDA=0&filialPDA={1}&idProcessoPDA=D&idDestino={2}";
                string request_url = String.Format(url, promax_address, unb, query);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Stream stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(stream);

                responseFromServer = reader.ReadToEnd();

                reader.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(String.Format("{0}-{1}", unb, ex.Message));
            }

            return responseFromServer.Split('=')[1].Trim().Replace('/', '\\');
        }

        static string QueryPromax(PromaxServer promax)
        {
            string unc = "";
            string promax_address = promax.PromaxServidor;
            int geo = Int16.Parse(promax.CodGeo);
            int unb = Int16.Parse(promax.CodUnb);
            try
            {
                Regex rx = new Regex(@"(?<word>[0-9.]*)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

                // Find matches.
                MatchCollection matches = rx.Matches(promax_address);

                // Report the number of matches found.
                //Console.WriteLine("{0} matches found in:\n   {1}", matches.Count, text);

                // Report on each match.            
                foreach (Match match in matches)
                {
                    GroupCollection groups = match.Groups;
                    if (groups["word"].Value.Length > 0)
                        promax_address = groups["word"].Value;
                }

                unc = "\\\\{0}\\integ\\{1:0000}\\descarga\\";
            }
            catch (Exception)
            {
                LogMessage(903, geo, unb, String.Format("Erro: Carregando arquivo de configuracao: Carrega configurações do SIV: QueryPromax(PromaxServer promax)"));
            }

            return String.Format(unc, promax_address, unb);
        }

        public static string PromaxPath(PromaxServer promax)
        {
            //return QueryPromax(promax, 1);
            return QueryPromax(promax);
        }

        public string SivPath(PromaxServer promax)
        {
            return QueryPromax(promax, 5);
        }

        List<string> getListGeos(List<PromaxServer> listSiv)
        {
            List<string> result = new List<string>();
            foreach (PromaxServer i in listSiv)
            {

                string found = result.Find(
                    delegate(string g)
                    {
                        return g == i.CodGeo;
                    }
                );

                if (found == null)
                {
                    result.Add(i.CodGeo);
                }
            }

            return result;
        }

        List<PromaxServer> getListPromax(DBHost OPMOB)
        {
            List<PromaxServer> result = new List<PromaxServer>();
            try
            {
                SqlConnection conn = new SqlConnection(OPMOB.ConnectionString);
                conn.Open();
                try
                {
                    string sql =
                        "SELECT" +
                        "  UNB.COD_UNB_SUP AS COD_GEO," +
                        "  SRV.COD_UNB AS COD_UNB," +
                        "  SRV.NOM_SERVIDOR AS NOM_SERVIDOR," +
                        "  SRV.NOM_BASE_DADO AS NOM_BASE_DADO," +
                        "  SRV.NOM_SENHA_BASE_DADO AS NOM_SENHA_BASE_DADO," +
                        "  SRV.NOM_USUARIO_BASE_DADO AS NOM_USUARIO_BASE_DADO," +
                        "  SRV.NOM_SERVIDOR_GERADOR AS NOM_SERVIDOR_GERADOR," +
                        "  SPR.DSC_CONTEUDO AS PROMAX_SERVER " +
                        "FROM" +
                        "  UNIDADE_BASICA_MB UNB," +
                        "  SERVIDOR_MB SRV," +
                        "  SISTEMA_MB SIS," +
                        "  SERVIDOR_PARAMETRO SPR " +
                        "WHERE" +
                        "  UNB.COD_UNB = SRV.COD_UNB AND" +
                        "  SPR.NOM_PARAMETRO = 'PARAM_CRITICA_ON_REQUISICAO' AND" +
                        "  SPR.COD_UNB = SRV.COD_UNB AND" +
                        "  SRV.COD_SISTEMA = SPR.COD_SISTEMA AND" +
                        "  SRV.COD_SISTEMA = SIS.COD_SISTEMA AND" +
                        "  SIS.NOM_SISTEMA = 'Promax' AND" +
                        "  SRV.DAT_EXC IS NULL " +
                        "ORDER BY 1, 2";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        PromaxServer item = new PromaxServer();
                        item.CodGeo = reader["COD_GEO"].ToString().Trim();
                        item.CodUnb = reader["COD_UNB"].ToString().Trim();
                        item.NomServidor = reader["NOM_SERVIDOR"].ToString().Trim();
                        item.NomBaseDado = reader["NOM_BASE_DADO"].ToString().Trim();
                        item.NomSenhaBaseDado = reader["NOM_SENHA_BASE_DADO"].ToString().Trim();
                        item.NomUsuarioBaseDado = reader["NOM_USUARIO_BASE_DADO"].ToString().Trim();
                        item.NomServidorGerador = reader["NOM_SERVIDOR_GERADOR"].ToString().Trim();
                        item.PromaxServidor = reader["PROMAX_SERVER"].ToString().Trim();
                        result.Add(item);
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
            catch
            {
                LogMessage(903, 0, 0, String.Format("Erro ao conectar com banco de dados! ({0}, {1})", OPMOB.Host, OPMOB.Database));
                return null;
            }

            return result;
        }

        string CodificarBase64(string texto)
        {
            string codificada = "";
            try
            {
                if (texto.Length > 0)
                {
                    byte[] encData_byte = new byte[texto.Length];
                    encData_byte = System.Text.Encoding.UTF8.GetBytes(texto);
                    codificada = Convert.ToBase64String(encData_byte);
                }
            }
            catch (Exception e)
            {
                LogMessage(900, 0, 0, e.Message);
                return texto;
            }

            return codificada;
        }

        string DecodificarBase64(string texto)
        {
            string m_decodificada = "";

            System.Text.UTF8Encoding m_encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder m_utf8Decode = m_encoder.GetDecoder();
            try
            {
                if (texto.Length > 0)
                {
                    byte[] m_todecode_byte = Convert.FromBase64String(texto);
                    int m_charCount = m_utf8Decode.GetCharCount(m_todecode_byte, 0, m_todecode_byte.Length);
                    char[] m_decoded_char = new char[m_charCount];
                    m_utf8Decode.GetChars(m_todecode_byte, 0, m_todecode_byte.Length, m_decoded_char, 0);
                    m_decodificada = new String(m_decoded_char);
                }
            }
            catch (Exception e)
            {
                LogMessage(900, 0, 0, e.Message);
                return texto;
            }

            return m_decodificada;
        }

        bool DeleteFile(string f)
        {
            if (System.IO.File.Exists(f))
            {
                System.IO.File.Delete(f);
                return true;
            }
            return false;
        }

        public bool DeleteFileNotRaise(string f)
        {
            bool result = false;
            if (System.IO.File.Exists(f))
            {
                try
                {
                    DeleteFile(f);
                    result = true;
                }
                catch //(Exception e)
                {
                    //Console.WriteLine(String.Format("Erro ao tentar excluir arquivo {0} ({1})", f, e.Message));
                    //Console.WriteLine(String.Format("{0:dd-MM-yyyy HH:mm:ss,FFFFFFF}|{1}|XXXXX|{2:000}|{3:0000}|{4}|{5:dd-MM-yyyy HH:mm:ss,FFFFFFF}|{6}|{7}", DateTime.Now, hostname, evento, cdd, Message, 0, area, setor));
                    result = false;
                }
            }
            return result;
        }

        public bool LoadConfig()
        {
            string user = "", pass = "", excluded_cdds = "";
            LogMessage(3, 0, 0, "Carregando configurações");
            try
            {
                try
                {
                    if (loader == null)
                        loader = new STPromaxIniLoader();
                    if (!System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "stpromax.ini"))
                    {
                        LogMessage(903, 0, 0, String.Format("Erro: O arquivo {0} não existe!", AppDomain.CurrentDomain.BaseDirectory + "sdsiv.ini"));
                    }
                    loader.ReadSTPromaxIniFile(AppDomain.CurrentDomain.BaseDirectory + "stpromax.ini");

                    //config = loader.Config();
                    user = Config().OPMOB.User;
                    pass = Config().OPMOB.Pass;
                    Config().OPMOB.User = DecodificarBase64(user);
                    Config().OPMOB.Pass = DecodificarBase64(pass);

                    try
                    {
                        //Carrega configurações do banco de dados OPMOB
                        Config().sivs = getListPromax(Config().OPMOB);
                    }
                    catch (Exception)
                    {
                        LogMessage(903, 0, 0, String.Format("Erro: Carregando arquivo de configuracao: Carrega configurações do banco de dados OPMOB"));
                    }

                    try
                    {
                        //Remove CDD's que não devem ser processados
                        RemoveExceptionList(Config().sivs, Config().CDDExclusion);
                        //Guarda quantidade de geos a serem processadas.
                        Config().geos = getListGeos(Config().sivs);
                    }
                    catch (Exception)
                    {
                        LogMessage(903, 0, 0, String.Format("Erro: Carregando arquivo de configuracao: Remove CDD's que não devem ser processados"));
                    }
                }
                finally
                {
                    LogMessage(3, 0, 0, "Configurações carregadas");
                    LogMessage(3, 0, 0, String.Format("AFARIA PATH: {0}", Config().AfariaPath));
                    LogMessage(3, 0, 0,
                        String.Format(
                            "OPMOB: HOST={0};PORT={1};USER={2};PASS={3};DATABASE={4}",
                            Config().OPMOB.Host,
                            Config().OPMOB.Port,
                            user,
                            pass,
                            Config().OPMOB.Database
                        )
                    );
                    excluded_cdds = "";
                    foreach (int cdd in Config().CDDExclusion)
                        excluded_cdds += cdd.ToString() + ";";
                    LogMessage(3, 0, 0, String.Format("LISTA DE CDD's EXCLUIDOS DO PROCESSO: {0}", excluded_cdds));
                    foreach (PromaxServer siv in Config().sivs)
                    {
                        LogMessage(3, 0, 0,
                            String.Format(
                                "LISTA DE CDD's DO PROCESSO: UNB={0:D4}; PROMAX={1}",
                                Int32.Parse(siv.CodUnb),
                            //siv.SIVServidorDescarga,
                                siv.PromaxServidorDescarga
                            )
                        );
                    }
                    LogMessage(3, 0, 0, String.Format("PROCESSOS PARALELOS: {0}", Config().ProcessosParalelos));
                }
            }
            catch (Exception e)
            {
                LogMessage(903, 0, 0, String.Format("Ocorreu um erro ao carregar as configurações ({0})", e.Message));
                return false;
            }

            return true;
        }
        /*
        public bool CompressLogFiles()
        {
            string[] files;
            string LogPath;
            string logfilename = LogFileName();

            LogPath = AppDomain.CurrentDomain.BaseDirectory;
            files = Directory.GetFiles(LogPath, "*.log");
            foreach (string zipfile in files)
            {
                if (zipfile == logfilename)
                    continue;
                try
                {
                    string anyString = File.ReadAllText(zipfile);
                    LogMessage(4, 0, 0, String.Format("Compactando aquivo ({0} => {0}.gz)", zipfile));
                    if(CompressStringToFile(zipfile + ".gz", anyString))
                        LogDeleteFile(0, 0, 0, 0, zipfile);
                    else
                        LogMessage(4, 0, 0, String.Format("Abortando compactar aquivo ({0} => {0}.gz)", zipfile));
                }
                catch (Exception e)
                {
                    LogMessage(904, 0, 0, String.Format("Ocorreu erro ao compactar aquivo {1} ({0})", e.Message, zipfile));

                    return false;
                }

                if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "stop"))
                    return true;
            }

            return true;
        }

        bool CompressStringToFile(string fileName, string value)
        {
            if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "stop"))
                return false;

            // A.
            // Write string to temporary file.
            string temp = Path.GetTempFileName();
            File.WriteAllText(temp, value);

            if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "stop"))
                return false;

            // B.
            // Read file into byte array buffer.
            byte[] b;
            using (FileStream f = new FileStream(temp, FileMode.Open))
            {
                b = new byte[f.Length];
                f.Read(b, 0, (int)f.Length);
            }

            if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "stop"))
                return false;

            // C.
            // Use GZipStream to write compressed bytes to target file.
            using (FileStream f2 = new FileStream(fileName, FileMode.Create))
            using (GZipStream gz = new GZipStream(f2, CompressionMode.Compress, false))
            {
                gz.Write(b, 0, b.Length);
            }

            return true;
        }
    }
    */
        public class STPromaxIniLoader
        {

            [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileStringW",
                SetLastError = true,
                CharSet = CharSet.Unicode, ExactSpelling = true,
                CallingConvention = CallingConvention.StdCall)]
            private static extern int GetPrivateProfileString(
                string lpAppName,
                string lpKeyName,
                string lpDefault,
                string lpReturnString,
                int nSize,
                string lpFilename);

            [DllImport("KERNEL32.DLL", EntryPoint = "WritePrivateProfileStringW",
                SetLastError = true,
                CharSet = CharSet.Unicode, ExactSpelling = true,
                CallingConvention = CallingConvention.StdCall)]
            private static extern int WritePrivateProfileString(
                string lpAppName,
                string lpKeyName,
                string lpString,
                string lpFilename);


            /// <summary>
            /// Gets the content.
            /// </summary>
            /// <param name="iniFile">The ini file.</param>
            /// <param name="category">The category.</param>
            /// <param name="key">The key.</param>
            /// <param name="defaultValue">The default value.</param>
            /// <returns></returns>
            private string GetIniFileString(string iniFile, string category, string key, string defaultValue)
            {
                string returnString = new string(' ', 1024);

                GetPrivateProfileString(category, key, defaultValue, returnString, 1024, iniFile);

                return returnString.Split('\0')[0];
            }

            /// <summary>
            /// Gets the keys.
            /// </summary>
            /// <param name="iniFile">The ini file.</param>
            /// <param name="category">The category.</param>
            private List<string> GetKeys(string iniFile, string category)
            {
                string returnString = new string(' ', 32768);

                GetPrivateProfileString(category, null, null, returnString, 32768, iniFile);
                List<string> result = new List<string>(returnString.Split('\0'));
                result.RemoveRange(result.Count - 2, 2);

                return result;
            }

            /// <summary>
            /// Gets the categories.
            /// </summary>
            /// <param name="iniFile">The ini file.</param>
            /// <returns></returns>
            private List<string> GetCategories(string iniFile)
            {
                string returnString = new string(' ', 65536);

                GetPrivateProfileString(null, null, null, returnString, 65536, iniFile);
                List<string> result = new List<string>(returnString.Split('\0'));
                result.RemoveRange(result.Count - 2, 2);

                return result;
            }

            /// <summary>
            /// Properties.
            /// </summary>        
            private STPromaxConfig m_config;

            public STPromaxIniLoader()
            {
                m_config = new STPromaxConfig();

                m_config.AfariaPath = "";
                m_config.OPMOB = new DBHost();
                m_config.sivs = null;
                m_config.CDDExclusion = new List<int>();
                m_config.ProcessosParalelos = 8; //Quantidade padrão de processos paralelos
            }

            public void ReadSTPromaxIniFile(string iniFile)
            {
                int icdd;
                int iprocparalelo;
                string procparalelo;
                string defaultValue = "Não encontrado!";
                List<string> categories = GetCategories(iniFile);
                List<string> keys = GetKeys(iniFile, "STPromax");
                string[] cdds = GetIniFileString(iniFile, "STPromax", "EXCLUSAO_CDD", defaultValue).Split(';');
                m_config.CDDExclusion.Clear();
                foreach (string cdd in cdds)
                    if (Int32.TryParse(cdd, out icdd))
                        m_config.CDDExclusion.Add(icdd);
                m_config.AfariaPath = GetIniFileString(iniFile, "STPromax", "AFARIA", defaultValue);
                m_config.OPMOB.Host = GetIniFileString(iniFile, "STPromax", "DB_HOST", defaultValue);
                m_config.OPMOB.Port = GetIniFileString(iniFile, "STPromax", "DB_PORT", defaultValue);
                m_config.OPMOB.Database = GetIniFileString(iniFile, "STPromax", "DB_DATABASE", defaultValue);
                m_config.OPMOB.User = GetIniFileString(iniFile, "STPromax", "DB_USER", defaultValue);
                m_config.OPMOB.Pass = GetIniFileString(iniFile, "STPromax", "DB_PASS", defaultValue);
                procparalelo = GetIniFileString(iniFile, "STPromax", "PROCESSOS_PARALELOS", defaultValue);
                if (Int32.TryParse(procparalelo, out iprocparalelo))
                {
                    if (iprocparalelo <= 0 || iprocparalelo > 64)
                        m_config.ProcessosParalelos = 8; //Valor padrão
                    else
                        m_config.ProcessosParalelos = iprocparalelo;
                }
                else
                    m_config.ProcessosParalelos = 8; //Valor padrão
            }

            public STPromaxConfig Config()
            {
                return m_config;
            }
        };

        public class HostName
        {
            public string Host { get; set; }
        };

        public class DBHost : HostName
        {
            public string User { get; set; }
            public string Pass { get; set; }
            public string Database { get; set; }
            public string Port { get; set; }
            public string ConnectionString
            {
                get
                {
                    string cns;

                    if (User == null)
                        User = "consultsv";
                    if (Pass == null)
                        Pass = "Kdowhs@";
                    if (Database == null)
                        Database = "OPVD";
                    if (Host == null)
                        Host = "127.0.0.1";

                    if (Port != null && Port.Length > 0)
                        cns =
                          "user id=" + User + ";" +
                          "Password=" + Pass + ";" +
                          "Network Address=" + Host + "," +
                          Port + ";" +
                          "Trusted_Connection=false;" +
                          "Database=" + Database + "; " +
                          "connection timeout=30";
                    else
                        cns =
                          "user id=" + User + ";" +
                          "Password=" + Pass + ";" +
                          "Network Address=" + Host + ";" +
                          "Trusted_Connection=false;" +
                          "Database=" + Database + "; " +
                          "connection timeout=30";

                    return cns;
                }
            }
        };

        public class PromaxServer
        {
            private string prxDescarga = "";
            public string CodGeo { get; set; }
            public string CodUnb { get; set; }
            public string NomServidor { get; set; }
            public string NomBaseDado { get; set; }
            public string NomSenhaBaseDado { get; set; }
            public string NomUsuarioBaseDado { get; set; }
            public string NomServidorGerador { get; set; }
            public string PromaxServidor { get; set; }
            public string PromaxServidorDescarga { get { if (prxDescarga.Length == 0) prxDescarga = STPromaxConsole.PromaxPath(this); return prxDescarga; } }
            public string SIVServidorDescarga { get { return ""; } }
        };

        public class STPromaxConfig
        {
            public int ProcessosParalelos { get; set; }
            public DBHost OPMOB { get; set; }
            public string AfariaPath { get; set; }
            public List<PromaxServer> sivs { get; set; }
            public List<string> geos { get; set; }
            public List<int> CDDExclusion { get; set; }
        };
    }
}
